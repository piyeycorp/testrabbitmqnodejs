import { CommandHandler, EventBus } from "@nestjs/cqrs";
import { CreateListRecordCommand } from "../commands/create-list-record.command";
import { ListRecordCreatedEvent } from "../events/list-record-created.event";

@CommandHandler(CreateListRecordCommand)
export class CreateListRecordHandler {
    constructor(private readonly eventBus: EventBus) { }

    async execute(command: CreateListRecordCommand) {
        console.log('CreateListRecordHandler: ', command);
        // Call repository to persist new record

        // Publish inMemory event
        this.eventBus.publish(
            new ListRecordCreatedEvent(command.recordId, command.listId)
        );
    }

}