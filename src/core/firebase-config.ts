import admin from 'firebase-admin';
// import  serviceAccount from './contollo-pes-dev-firebase-adminsdk-xhjz0-604b76af6e.json'
// const serviceAccount = require("./contollo-pes-dev-firebase-adminsdk-xhjz0-604b76af6e.json");
// import serviceAccount = require("./contollo-pes-dev-firebase-adminsdk-xhjz0-604b76af6e.json");

export class FirebaseConfig {
  private static instance: FirebaseConfig;
  private pesApp: admin.app.App;
  private storage: admin.storage.Storage;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() { }

  public static getInstance(): FirebaseConfig {
    if (!FirebaseConfig.instance) {
      FirebaseConfig.instance = new FirebaseConfig();
      const firebaseConfig = JSON.parse(process.env.FIREBASE_CONFIG ?? "{}");
      FirebaseConfig.instance.pesApp = admin.initializeApp(
        {
          credential: admin.credential.cert("./src/core/contollo-pes-dev-firebase-adminsdk-xhjz0-604b76af6e.json"),
          databaseURL: 'https://contollo-pes-dev.firebaseapp.com',
          storageBucket: firebaseConfig.storageBucket
        },
        'PES_APP',
      );
      // FirebaseConfig.instance.pesApp = admin.initializeApp(
      //   { storageBucket: firebaseConfig.storageBucket }, 'PES_APP');

      // FirebaseConfig.instance.storage = admin.storage(FirebaseConfig.instance.pesApp);
    }
    return FirebaseConfig.instance;
  }

  public static get auth(): admin.auth.Auth {
    return FirebaseConfig.getInstance().pesApp.auth();
  }

  public static getMessaging() {
    return this.getInstance().pesApp.messaging();
  }

  public static firestore(): FirebaseFirestore.Firestore {
    return FirebaseConfig.getInstance().pesApp.firestore();
  }

  public static storage() {
    return FirebaseConfig.getInstance().storage;
  }

  public static get collectionsCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('Collections');
  }

  public static get dataCollectionsCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('DataCollections');
  }

  public static get hierarchiesCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('Hierarchies');
  }

  public static get nodeManager(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('NodeManager');
  }

  public static get processFlowsCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('ProcessFlows');
  }

  public static get workOrdersCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('WorkOrders');
  }

  public static get notificationEmailCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('NotificationEmails');
  }

  public static get formsCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('Forms');
  }

  public static get formListMappingCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('FormListMapping');
  }
  public static get formSubmissionsCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('FormSubmissions');
  }

  public static get usersCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('Users');
  }

  public static get emailVerificationCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('EmailVerification');
  }

  public static get rolesCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('Roles');
  }

  public static get groupsCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('Groups');
  }

  public static get permissionsCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('Permissions');
  }

  public static get templateListsCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('TemplateList');
  }

  public static get hierarchySort(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('HierarchiesSort');
  }

  public static get tasksCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('Tasks');
  }

  public static get listPermissions(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('ListPermissions');
  }

  public static get dataManagerPermissions(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('DataManagerPermissions');
  }

  public static get userTokenCollection(): FirebaseFirestore.CollectionReference {
    return FirebaseConfig.firestore().collection('UserToken');
  }

}
