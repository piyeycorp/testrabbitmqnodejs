export class ListRecordCreatedEvent {
    // recordId: string;
    // listId: string;
    constructor(public recordId: string, public listId: string) { }
}