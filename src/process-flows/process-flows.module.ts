// import * as redisStore from 'cache-manager-redis-store';
import { CacheModule, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { ProcessFlowsService } from './services/process-flows.service';
import { ProcessFlowsRepository } from './repositories/process-flows.repository';
import { ProcessFlowsController } from './controllers/process-flows.controller';
import { UpdateProcessFlowHandler } from './handlers/update-process-flow.handler';
import { ProcessFlowUpdatedHandler } from './handlers/process-flow-updated.handler';
import { AppGateway } from 'src/app.gateway';

export const CommandHandlers = [UpdateProcessFlowHandler];
export const QueryHandlers = [];
export const EventHandlers = [ProcessFlowUpdatedHandler];

@Module({
  imports: [
    CqrsModule,
    CacheModule.register({
      // Connect to redist on cloud
      // store: redisStore,
      // host: 'redis-12489.c273.us-east-1-2.ec2.cloud.redislabs.com',
      // port: 12489,
      // password: 'tY7xuRVttmlZ0W81OPO9y16JA9b1QCRG',

      // Connect to redist locally
      // host: 'localhost',
      // port: 6379,
      // ttl: 60,
    })
  ],
  controllers: [ProcessFlowsController],
  providers: [
    ProcessFlowsService,
    ProcessFlowsRepository,
    AppGateway,
    ...CommandHandlers,
    ...QueryHandlers,
    ...EventHandlers,
  ]
})
export class ProcessFlowsModule { }
