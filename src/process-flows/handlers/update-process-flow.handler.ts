import { CommandHandler, EventBus } from "@nestjs/cqrs";
import { UpdateProcessFlowCommand } from "../commands/update-process-flow.command";
import { ProcessFlowUpdatedEvent } from "../events/process-flow-updated.event";
import { ProcessFlowsRepository } from "../repositories/process-flows.repository";

@CommandHandler(UpdateProcessFlowCommand)
export class UpdateProcessFlowHandler {

    constructor(
        private readonly eventBus: EventBus,
        private readonly processFlowsRepository: ProcessFlowsRepository,
    ) { }

    async execute(command: UpdateProcessFlowCommand) {
        console.log('UpdateProcessFlowCommand: ', command);
        const updateProcessFlow = {
            name: command.name
        };
        await this.processFlowsRepository.update(command.processFlowId, updateProcessFlow);

        // Publish inMemory event
        this.eventBus.publish(
            new ProcessFlowUpdatedEvent(command.processFlowId)
        );
    }

}