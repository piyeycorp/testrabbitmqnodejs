import { Inject, Logger, OnApplicationBootstrap } from "@nestjs/common";
import { EventsHandler, IEventHandler } from "@nestjs/cqrs";
import { ClientProxy } from "@nestjs/microservices";
import { lastValueFrom } from 'rxjs';
import { CreateWorkOrderCommand } from "../../work-orders/commands/create-work-order.command";
import { ListRecordCreatedEvent } from "../events/list-record-created.event";

@EventsHandler(ListRecordCreatedEvent) // This is used for inMemory Event handlers
export class ListRecordCreatedHandler implements IEventHandler<ListRecordCreatedEvent>, OnApplicationBootstrap {

    private logger: Logger = new Logger('AppGateway');

    constructor(@Inject('WORK_ORDERS_SERVICE_RMQ') private readonly client: ClientProxy) { }
    // constructor(@Inject('WORK_ORDERS_SERVICE_REDIS') private readonly client: ClientProxy) { }

    handle(event: ListRecordCreatedEvent) {
        console.log('ListRecordCreatedHandler -> event: ', event);

        // Validate if the list has a trigger
        //      if it has, execute the logic for decisions
        //          if all conditionals are met, send an event to the queue

        // Simulating the logic for creating a WorkOrder or not
        const createWorkOrder = Math.random() < 0.8; // 80% probability of getting true
        if (createWorkOrder) {
            const createWorkOrderCommand = new CreateWorkOrderCommand(event.recordId, event.listId);
            console.log('ListRecordCreatedHandler -> Sending ', createWorkOrderCommand);
            // Send a message to the queue
            const response = this.client.send('createWorkOrderCommand', createWorkOrderCommand);
            lastValueFrom(response)
                .then(data => console.log(data))
                .catch(error => console.log(error)); // since toPromise method is deprecated, we use lastValueFrom, so the message is sent
        }
    }

    onApplicationBootstrap() {
        console.log('Connecting ClientProxy');
        this.client
            .connect()
            .then(data => this.logger.log(`Connected to ClientProxy. ${data}`))
            .catch(reason => this.logger.error(`And error occurred. ${reason}`));
    }
}