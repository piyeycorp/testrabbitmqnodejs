import { Module } from '@nestjs/common';
import { ListsService } from './services/lists.service';
import { ListsController } from './controllers/lists.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CreateListRecordHandler } from './handlers/create-list-record.handler';
import { ListRecordCreatedHandler } from './handlers/list-record-created.handler';
import { CqrsModule } from '@nestjs/cqrs';

export const CommandHandlers = [CreateListRecordHandler];
export const QueryHandlers = [];
export const EventHandlers = [ListRecordCreatedHandler];
@Module({
  imports: [
    CqrsModule,
    ClientsModule.register([
      {
        name: 'WORK_ORDERS_SERVICE_RMQ',
        transport: Transport.RMQ,
        options: {
          urls: ['amqps://swcdjxnu:l2CN8d-f-6CYNWW9x1gTVxmqb5HRXMeA@beaver.rmq.cloudamqp.com/swcdjxnu'],
          queue: 'workorders_queue',
          // noAck: false, // false to manual acknowledgment mode
          queueOptions: {
            durable: false
          },
        },
      },
      {
        name: 'WORK_ORDERS_SERVICE_REDIS',
        transport: Transport.REDIS,
        options: {
          // url: 'redis-12489.c273.us-east-1-2.ec2.cloud.redislabs.com:12489',
          // host: 'redis-12489.c273.us-east-1-2.ec2.cloud.redislabs.com',
          // port: 12489,
          // password: 'tY7xuRVttmlZ0W81OPO9y16JA9b1QCRG',
          host: 'localhost',
          port: 6379,
          retryAttempts: 5,
          retryDelay: 100
        },
      },
    ]),
  ],
  controllers: [ListsController],
  providers: [
    ListsService,
    ...CommandHandlers,
    ...QueryHandlers,
    ...EventHandlers,
  ]
})
export class ListsModule { }
