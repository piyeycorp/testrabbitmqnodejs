import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ProcessFlowsService } from '../services/process-flows.service';
import { CreateProcessFlowDto } from '../dto/create-process-flow.dto';
import { UpdateProcessFlowDto } from '../dto/update-process-flow.dto';

@Controller('process-flows')
export class ProcessFlowsController {
  constructor(private readonly processFlowsService: ProcessFlowsService) { }

  @Post()
  create(@Body() createProcessFlowDto: CreateProcessFlowDto) {
    return this.processFlowsService.create(createProcessFlowDto);
  }

  @Get()
  findAll() {
    return this.processFlowsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.processFlowsService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProcessFlowDto: UpdateProcessFlowDto) {
    return this.processFlowsService.update(id, updateProcessFlowDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.processFlowsService.remove(id);
  }
}
