import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { CreateListRecordCommand } from '../commands/create-list-record.command';
import { CreateListRecordDto } from '../dto/create-list-record.dto';
import { CreateListDto } from '../dto/create-list.dto';
import { UpdateListDto } from '../dto/update-list.dto';

@Injectable()
export class ListsService {
  constructor(private readonly commandBus: CommandBus) { }

  create(createListDto: CreateListDto) {
    console.log(createListDto);
    return 'This action adds a new list';
  }

  findAll() {
    return `This action returns all lists`;
  }

  findOne(id: number) {
    return `This action returns a #${id} list`;
  }

  update(id: number, updateListDto: UpdateListDto) {
    console.log(updateListDto);
    return `This action updates a #${id} list`;
  }

  remove(id: number) {
    return `This action removes a #${id} list`;
  }

  addRecord(listId: string, record: CreateListRecordDto) {
    // Add logic in Application Service

    // Si algo no cumple logica, enviar un error

    const createListRecordCommand = new CreateListRecordCommand(record.recordId, listId);
    console.log('ListsService. Sending CreateListRecordCommand:', createListRecordCommand);
    return this.commandBus.execute(createListRecordCommand);

    // return `This action will add a new record #${record.recordId} to a list #${listId}`;
  }
}
