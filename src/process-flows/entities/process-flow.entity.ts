import { Activities, Diagram } from "./activity.entity";

export type ProcessFlows = ProcessFlow[];

export class ProcessFlow {
    public id: string;
    public name: string;
    public activities?: Activities;
    public formIds?: string[];
    public diagramData?: string;
    public diagram?: Diagram;
    public valid: boolean;
    // public createdBy: string;
    // public createdAt: FirebaseFirestore.Timestamp | FirebaseFirestore.FieldValue;
    // public updatedBy?: string;
    // public updatedAt?: FirebaseFirestore.Timestamp | FirebaseFirestore.FieldValue;
}