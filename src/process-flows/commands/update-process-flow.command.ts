export class UpdateProcessFlowCommand {
    constructor(public processFlowId: string, public name: string) { }
}