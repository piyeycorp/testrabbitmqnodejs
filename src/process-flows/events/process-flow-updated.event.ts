export class ProcessFlowUpdatedEvent {
    constructor(public processFlowId: string) { }
}