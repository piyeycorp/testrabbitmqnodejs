import { Logger } from '@nestjs/common';
import { OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ProcessFlowDto } from './process-flows/dto/process-flow.dto';

@WebSocketGateway({ path: '/ps.websockets' })
export class AppGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

  private logger: Logger = new Logger('AppGateway');
  @WebSocketServer()
  webSocketServer: Server

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  afterInit(server: Server) {
    this.logger.log('WebSocketGateway Initialized.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  handleConnection(client: Socket, ...args: any[]) {
    this.logger.log(`Client connected: ${client.id}`);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  // @SubscribeMessage('msgToServer')
  // // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // handleMessage(client: Socket, text: string): WsResponse<string> {
  //   return { event: 'msgToClient', data: text };
  // }

  @SubscribeMessage('msgToServer')
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  handleMessage(client: Socket, text: string): void {
    this.webSocketServer.emit('msgToClient', text);
  }

  sendProcessFlowUpdatedEvent(processFlowsDto: ProcessFlowDto[]): void {
    this.webSocketServer.emit('ProcessFlowUpdatedEvent', processFlowsDto);
  }
}
