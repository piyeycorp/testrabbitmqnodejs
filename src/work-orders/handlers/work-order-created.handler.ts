import { EventsHandler, IEventHandler } from "@nestjs/cqrs";
import { WorkOrderCreatedEvent } from "../events/work-order-created.event";

@EventsHandler(WorkOrderCreatedEvent) // This is used for inMemory Event handlers
export class WorkOrderCreatedHandler implements IEventHandler<WorkOrderCreatedEvent> {

    handle(event: WorkOrderCreatedEvent) {
        // Add logic for Work Order created in WorkOrder domain
        console.log('WorkOrderCreatedHandler -> ', event);
    }

}