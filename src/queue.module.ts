import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CreateWorkOrderHandler } from './work-orders/handlers/create-work-order.handler';
// import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
    imports: [
        CqrsModule,
        // ClientsModule.register([
        //     { name: 'WORK_ORDERS_SERVICE_RMQ', transport: Transport.RMQ }
        // ])
    ],
    controllers: [CreateWorkOrderHandler]
})
export class QueueModule { }
