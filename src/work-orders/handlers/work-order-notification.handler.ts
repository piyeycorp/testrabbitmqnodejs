import { EventsHandler, IEventHandler } from "@nestjs/cqrs";
import { WorkOrderCreatedEvent } from "../events/work-order-created.event";

@EventsHandler(WorkOrderCreatedEvent) // This is used for inMemory Event handlers
export class WorkOrderNotificationHandler implements IEventHandler<WorkOrderCreatedEvent> {

    handle(event: WorkOrderCreatedEvent) {
        // Add logic for Sending notifications
        console.log('WorkOrderNotificationHandler -> ', event);

        const sendEmail = Math.random() < 0.75; // 75% probability of getting true
        if (sendEmail) {
            // Send a command to send an email
            console.log('WorkOrderNotificationHandler -> Sending Email')
        }

        const sendAppNotification = Math.random() < 0.75; // 75% probability of getting true
        if (sendAppNotification) {
            // Send a command to send an email
            console.log('WorkOrderNotificationHandler -> Sending App Notification')
        }
    }

}