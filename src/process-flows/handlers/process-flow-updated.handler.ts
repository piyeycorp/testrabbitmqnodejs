import { CACHE_MANAGER, Inject } from "@nestjs/common";
import { EventsHandler } from "@nestjs/cqrs";
import { Cache } from 'cache-manager';
import { AppGateway } from "src/app.gateway";
import { ProcessFlowDto } from "../dto/process-flow.dto";
import { ProcessFlowUpdatedEvent } from "../events/process-flow-updated.event";
import { ProcessFlowsRepository } from "../repositories/process-flows.repository";

@EventsHandler(ProcessFlowUpdatedEvent)
export class ProcessFlowUpdatedHandler {

    constructor(
        private readonly processFlowsRepository: ProcessFlowsRepository,
        private readonly appGateway: AppGateway,
        @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
    ) { }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async handle(command: ProcessFlowUpdatedEvent) {
        const processFlows = await this.processFlowsRepository.getAll();
        const processFlowsDto = processFlows.map(processFlow => {
            const processFlowDto: ProcessFlowDto = {
                id: processFlow.id,
                name: processFlow.name,
                activities: processFlow.activities,
                diagramData: processFlow.diagramData,
                diagram: processFlow.diagram,
                valid: processFlow.valid,
                // createdBy: users.find(u => u.userId == processFlow.createdBy),
                // createdAt: (processFlow.createdAt as FirebaseFirestore.Timestamp)?.toDate(),
                // updatedBy: users.find(u => u.userId == processFlow.updatedBy),
                // updatedAt: (processFlow.updatedAt as FirebaseFirestore.Timestamp)?.toDate(),
            };

            return processFlowDto;
        });
        await this.cacheManager.set('process-flows', processFlowsDto, { ttl: 300 });
        this.appGateway.sendProcessFlowUpdatedEvent(processFlowsDto);
    }

}