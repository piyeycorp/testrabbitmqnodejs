export class CreateWorkOrderCommand {
    constructor(public recordId: string, public listId: string) { }
}