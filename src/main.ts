import { NestFactory } from '@nestjs/core';
import { RedisOptions, RmqOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
// import { QueueModule } from './queue.module';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.useStaticAssets(join(__dirname, '..', 'static'));

  // Using the same app than the backend
  app.connectMicroservice<RmqOptions>({
    transport: Transport.RMQ,
    options: {
      urls: ['amqps://swcdjxnu:l2CN8d-f-6CYNWW9x1gTVxmqb5HRXMeA@beaver.rmq.cloudamqp.com/swcdjxnu'],
      queue: 'workorders_queue',
      noAck: false, // false to manual acknowledgment mode
      queueOptions: {
        durable: false
      },
    },
  });

  app.connectMicroservice<RedisOptions>({
    transport: Transport.REDIS,
    options: {
      // url: 'redis-12489.c273.us-east-1-2.ec2.cloud.redislabs.com:12489',
      // host: 'redis-12489.c273.us-east-1-2.ec2.cloud.redislabs.com',
      // port: 12489,
      // password: 'tY7xuRVttmlZ0W81OPO9y16JA9b1QCRG',
      host: 'localhost',
      port: 6379,
      retryAttempts: 5,
      retryDelay: 100
    },
  });
  app.startAllMicroservices();
  // End using the same app than the backend

  await app.listen(3000);


  // Using a separete app than the backend

  // const queue = await NestFactory.createMicroservice<MicroserviceOptions>(QueueModule, {
  //   transport: Transport.RMQ,
  //   options: {
  //     urls: ['amqps://swcdjxnu:l2CN8d-f-6CYNWW9x1gTVxmqb5HRXMeA@beaver.rmq.cloudamqp.com/swcdjxnu'],
  //     queue: 'workorders_queue',
  //     queueOptions: {
  //       durable: false
  //     },
  //   },
  // });
  // await queue.listen();

  // End using a separete app than the backend
}
bootstrap();
