import { Controller } from "@nestjs/common";
import { EventBus } from "@nestjs/cqrs";
import {
    Ctx,
    EventPattern,
    MessagePattern,
    Payload,
    RedisContext,
    RmqContext
} from "@nestjs/microservices";
import { Guid } from "src/core/guid";
import { CreateWorkOrderCommand } from "../commands/create-work-order.command";
import { WorkOrderCreatedEvent } from "../events/work-order-created.event";

@Controller() // Should be added as a controller, so NestJS can find the EventPattern
export class CreateWorkOrderHandler {

    constructor(private readonly eventBus: EventBus) { }

    /*
        Using RabbitMQ
    */
    @MessagePattern('createWorkOrderCommand')
    handle(@Payload() data: CreateWorkOrderCommand, @Ctx() context: RmqContext) {
        const channel = context.getChannelRef();
        const originalMsg = context.getMessage();

        const workOrderId = Guid.newGuid();

        console.log(`CreateWorkOrderHandler -> id:${workOrderId}, data: `, data);
        try {
            const fail = Math.random() < 0.2; // 20% probability of getting true
            if (fail) {
                throw new Error('something fail');
            }
            console.log('WorkOrder created');
            channel.ack(originalMsg);

            // Publish inMemory event
            this.eventBus.publish(
                new WorkOrderCreatedEvent(workOrderId, data.recordId, data.listId)
            );
            return workOrderId;
        } catch (error) {
            console.log(`Error creating WorkOrder: ${error}`);
            channel.reject(originalMsg);
        }
    }

    /*
        Using Redis
    */
    // @MessagePattern('createWorkOrderCommand')
    // async handle(@Payload() data: CreateWorkOrderCommand): Promise<string> {
    //     const workOrderId = Guid.newGuid();

    //     console.log(`CreateWorkOrderHandler -> id:${workOrderId}, data: `, data);
    //     try {
    //         const fail = Math.random() < 0.2; // 20% probability of getting true
    //         if (fail) {
    //             throw new Error('something fail');
    //         }
    //         console.log('WorkOrder created');

    //         // Publish inMemory event
    //         this.eventBus.publish(
    //             new WorkOrderCreatedEvent(workOrderId, data.recordId, data.listId)
    //         );
    //         return workOrderId;
    //     } catch (error) {
    //         console.log(`Error creating WorkOrder: ${error}`);
    //         return Promise.reject(error);
    //     }
    // }
}