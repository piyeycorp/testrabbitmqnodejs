export class CreateListRecordCommand {
    constructor(public recordId: string, public listId: string) { }
}