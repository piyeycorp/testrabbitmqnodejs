export class CreateListRecordDto {
    public recordId: string;
    public name: string;
}