import { Module } from '@nestjs/common';
import { WorkOrdersService } from './services/work-orders.service';
import { WorkOrdersController } from './controllers/work-orders.controller';
import { CreateWorkOrderHandler } from './handlers/create-work-order.handler';
import { WorkOrderCreatedHandler } from './handlers/work-order-created.handler';
import { CqrsModule } from '@nestjs/cqrs';
import { WorkOrderNotificationHandler } from './handlers/work-order-notification.handler';

export const CommandHandlers = [CreateWorkOrderHandler];
export const QueryHandlers = [];
export const EventHandlers = [WorkOrderCreatedHandler, WorkOrderNotificationHandler];
@Module({
  imports: [
    CqrsModule,
  ],
  controllers: [WorkOrdersController],
  providers: [
    WorkOrdersService,
    ...CommandHandlers,
    ...QueryHandlers,
    ...EventHandlers,
  ]
})
export class WorkOrdersModule { }
