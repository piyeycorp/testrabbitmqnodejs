import { Test, TestingModule } from '@nestjs/testing';
import { ListsController } from './lists.controller';
import { ListsService } from '../services/lists.service';
import { CommandBus } from '@nestjs/cqrs';

describe('ListsController', () => {
    let controller: ListsController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ListsController],
            providers: [ListsService, CommandBus],
        }).compile();

        controller = module.get<ListsController>(ListsController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
