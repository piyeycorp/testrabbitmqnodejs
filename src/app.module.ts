import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ListsModule } from './lists/lists.module';
import { QueueModule } from './queue.module';
import { WorkOrdersModule } from './work-orders/work-orders.module';
import { ProcessFlowsModule } from './process-flows/process-flows.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      ignoreEnvFile: true,
      isGlobal: true,
    }),
    QueueModule,
    ListsModule,
    WorkOrdersModule,
    ProcessFlowsModule // If using the same app than the backend, we need to import the QueueModule in here
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
