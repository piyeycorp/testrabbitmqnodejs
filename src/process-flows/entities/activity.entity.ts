export type Activities = Activity[];

export type activityType = 'start' | 'process' | 'decision' | 'list' | 'terminator';

export class Activity {
    id: string;
    name: string;
    type: activityType;
    nextActivities?: Array<string> | Array<any>;
    previousActivities?: Array<string>;
    formId?: string;
}

export interface Diagram {
    page: Page;
    connectors: Connector[];
    shapes: Shape[];
}

export interface Connector {
    key: string;
    dataKey: string;
    text: string;
    beginItemKey: string;
    endItemKey: string;
    beginConnectionPointIndex: number;
    endConnectionPointIndex: number;
    points: Point[];
    locked: boolean;
    zIndex: number;
}

export interface Point {
    x: number;
    y: number;
}

export interface Page {
    width: number;
    height: number;
    pageColor: number;
    pageWidth: number;
    pageHeight: number;
    pageLandscape: boolean;
    units: number;
}

export interface Shape {
    key: string;
    dataKey: string;
    locked: boolean;
    zIndex: number;
    type: string;
    text: string;
    x: number;
    y: number;
    width: number;
    height: number;
}