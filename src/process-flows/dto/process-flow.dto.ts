import { Activities, Diagram } from "../entities/activity.entity";

export interface ProcessFlowDto {
    id: string;
    name: string;
    activities?: Activities;
    diagramData?: string;
    diagram?: Diagram;
    valid: boolean;
    // createdBy: User;
    // createdAt: Date;
    // updatedBy?: User;
    // updatedAt?: Date;
}