import { Injectable } from "@nestjs/common";
import { FirebaseConfig } from "src/core/firebase-config";
import { ProcessFlow, ProcessFlows } from "../entities/process-flow.entity";

@Injectable()
export class ProcessFlowsRepository {


    private processFlowsCollection: FirebaseFirestore.CollectionReference<FirebaseFirestore.DocumentData>;

    constructor() {
        this.processFlowsCollection = FirebaseConfig.processFlowsCollection;
    }

    public async getAll(): Promise<ProcessFlows> {
        const querySnapshot = await this.processFlowsCollection
            .orderBy("createdAt")
            .get();
        const processFlows = querySnapshot.docs.map(doc => {
            const data = doc.data();
            const processFlow: ProcessFlow = {
                id: doc.id,
                name: data.name,
                formIds: data.formIds,
                activities: data.activities,
                diagramData: data.diagramData,
                diagram: data.diagram,
                valid: data.valid,
                // createdBy: data.createdBy,
                // createdAt: data.createdAt,
                // updatedBy: data.updatedBy,
                // updatedAt: data.updatedAt
            };
            return processFlow;
        });

        return processFlows;
    }

    public async update(id: string, processFlow: Partial<ProcessFlow>): Promise<void> {
        const doc = this.processFlowsCollection.doc(id);
        if (!(await doc.get()).exists) throw new Error(`Document ${id} does not exist`);

        await doc.update(processFlow);
    }
}