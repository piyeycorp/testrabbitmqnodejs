export class WorkOrderCreatedEvent {
    constructor(public workOrderId: string, public recordId: string, public listId: string) { }
}