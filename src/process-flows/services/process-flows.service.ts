import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { Cache } from 'cache-manager';
import { UpdateProcessFlowCommand } from '../commands/update-process-flow.command';
import { CreateProcessFlowDto } from '../dto/create-process-flow.dto';
import { ProcessFlowDto } from '../dto/process-flow.dto';
import { UpdateProcessFlowDto } from '../dto/update-process-flow.dto';
import { ProcessFlowsRepository } from '../repositories/process-flows.repository';

@Injectable()
export class ProcessFlowsService {
  constructor(
    private readonly processFlowsRepository: ProcessFlowsRepository,
    private readonly commandBus: CommandBus,
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
  ) { }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  create(createProcessFlowDto: CreateProcessFlowDto) {
    return 'This action adds a new processFlow';
  }

  async findAll() {
    const cachedProcessFlows = await this.cacheManager.get('process-flows');
    if (cachedProcessFlows) {
      console.log('Using cachedProcessFlows');
      return cachedProcessFlows;
    }
    console.log('No cachedProcessFlows, retrieving from database');

    const processFlows = await this.processFlowsRepository.getAll();
    const processFlowsDto = processFlows.map(processFlow => {
      const processFlowDto: ProcessFlowDto = {
        id: processFlow.id,
        name: processFlow.name,
        activities: processFlow.activities,
        diagramData: processFlow.diagramData,
        diagram: processFlow.diagram,
        valid: processFlow.valid,
        // createdBy: users.find(u => u.userId == processFlow.createdBy),
        // createdAt: (processFlow.createdAt as FirebaseFirestore.Timestamp)?.toDate(),
        // updatedBy: users.find(u => u.userId == processFlow.updatedBy),
        // updatedAt: (processFlow.updatedAt as FirebaseFirestore.Timestamp)?.toDate(),
      };

      return processFlowDto;
    });
    await this.cacheManager.set('process-flows', processFlowsDto, { ttl: 300 });
    return processFlowsDto;
  }

  findOne(id: string) {
    return `This action returns a #${id} processFlow`;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: string, updateProcessFlowDto: UpdateProcessFlowDto) {
    const updateProcessFlowCommand = new UpdateProcessFlowCommand(id, updateProcessFlowDto.name);
    return this.commandBus.execute(updateProcessFlowCommand);
  }

  remove(id: string) {
    return `This action removes a #${id} processFlow`;
  }
}
