import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ListsService } from '../services/lists.service';
import { CreateListDto } from '../dto/create-list.dto';
import { UpdateListDto } from '../dto/update-list.dto';
import { CreateListRecordDto } from '../dto/create-list-record.dto';

@Controller('lists')
export class ListsController {
  constructor(private readonly listsService: ListsService) { }

  @Post()
  create(@Body() createListDto: CreateListDto) {
    return this.listsService.create(createListDto);
  }

  @Get()
  findAll() {
    return this.listsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.listsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateListDto: UpdateListDto) {
    return this.listsService.update(+id, updateListDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.listsService.remove(+id);
  }

  @Post(':listId/data')
  addRecord(@Param('listId') listId: string, @Body() createListRecordDto: CreateListRecordDto) {
    console.log(`ListsController.addRecord() -> listId: ${listId}, createListRecordDto: `, createListRecordDto);
    return this.listsService.addRecord(listId, createListRecordDto);

    // We could avoid using an Application Service and send the command directly from the controller
    // const createListRecordCommand = new CreateListRecordCommand(record.recordId, listId);
    // return this.commandBus.execute(createListRecordCommand);
  }
}
